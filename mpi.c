#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

#ifndef ERROR
#define ERROR -1
#endif

int main(int argc, char  **argv)
{
	if (argc < 4)
	{
		printf("Please enter 3 values for a b and c respectively separated by space\n");
		return ERROR;
	}

	int npes, rank;
  int A, B, C, D;
  int multAB, funcBCA, funcA;

  MPI_Init(&argc, &argv);
  MPI_Status status;

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);

  if (rank == 0 )
  {
    A = atoi(argv[1]);
    MPI_Bcast(&A, 1, MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Recv(&multAB, 1, MPI_INT, 1, 20, MPI_COMM_WORLD, &status);
    MPI_Recv(&funcBCA, 1, MPI_INT, 2, 30, MPI_COMM_WORLD, &status);
    MPI_Recv(&funcA, 1, MPI_INT, 3, 40, MPI_COMM_WORLD, &status);

    D = multAB + funcBCA + funcA;
    printf("D=%d\n", D);
  }
  else if (rank == 1)
  {
    B = atoi(argv[2]);
    MPI_Bcast(&A, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Send(&B, 1, MPI_INT, 2, 10, MPI_COMM_WORLD);

    multAB = A * B;

    MPI_Send(&multAB, 1, MPI_INT, 0, 20, MPI_COMM_WORLD);
  }
  else if (rank == 2)
  {
    C = atoi(argv[3]);
    MPI_Bcast(&A, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Recv(&B, 1, MPI_INT, 1, 10, MPI_COMM_WORLD, &status);

    funcBCA = B/(C+A);

    MPI_Send(&funcBCA, 1, MPI_INT, 0, 30, MPI_COMM_WORLD);
  }
  else if (rank == 3)
  {
    MPI_Bcast(&A, 1, MPI_INT, 0, MPI_COMM_WORLD);

    funcA = (A-1)*(A-2);

    MPI_Send(&funcA, 1, MPI_INT, 0, 40, MPI_COMM_WORLD);
  }

  MPI_Finalize();
	return 0;
}
